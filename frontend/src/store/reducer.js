import {FETCH_POST_SUCCESS} from "./actions";


const initialState = {
    news: [],
    comments: []
};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POST_SUCCESS:
            return {...state, news: action.posts};
        default:
            return state
    }

};

export default reducer