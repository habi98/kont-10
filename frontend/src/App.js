import React, { Component, Fragment } from 'react';
import './App.css';
import Toolbar from "./compoments/Toolbar/Toolbar";
import {Container} from "reactstrap";
import {Route,  Switch} from "react-router-dom";
import NewPost from "./containers/NewPost/NewPost";
import Posts from "./containers/Posts/Posts";

class App extends Component {
  render() {
    return (
        <Fragment>
            <Toolbar/>
            <Container>
                <Switch>
                  <Route path="/" exact component={Posts}/>
                    <Route path="/posts/new"  component={NewPost}/>
                </Switch>
            </Container>
        </Fragment>

    );
  }
}

export default App;
