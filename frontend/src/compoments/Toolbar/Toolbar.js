import React from 'react';
import {Navbar, NavbarBrand} from "reactstrap";

const Toolbar = () => {
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand href="/">News</NavbarBrand>
        </Navbar>
    );
};

export default Toolbar;

