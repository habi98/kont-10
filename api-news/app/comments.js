const express = require('express');
const router = express.Router();


const createRouter = connection => {
    router.get('/', (req,res) => {
        connection.query('SELECT * FROM `comments`', (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'})
            }
            res.send(results)
        })
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `comments` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'})
            }
            if (results[0]) {
                res.send(results[0])
            } else {
                res.status(404).send({error: 'comment not found'})
            }
        })
    });

    router.post('/', (req,res) => {
        const comment = req.body;
        connection.query('INSERT INTO `comments` (`author, comment`) VALUES (?, ?),',
            [comment.author, comment.comment],
            (error) => {
              if(error) {
                  res.status(500).send({error: 'Database error'})
              }
              res.send({message: 'OK'})
            }
        )
    });

    router.delete('/:id', (req,res) => {
        connection.query('DELETE FROM `comments` WHERE `id` = ?', req.params.id, (error) => {
            if (!error) {
                res.send('Deleted successfully')
            } else {
                res.status(500).send({error: 'Deletion is impossible'})
            }

        })
    });

    return router;
};

module.exports = createRouter;