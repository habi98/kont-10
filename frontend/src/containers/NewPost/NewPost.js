import React, {Component, Fragment} from 'react';
import NewsAddForm from "../../compoments/NewsAddForm/NewsAddForm";
import {Container} from "reactstrap";
import {connect} from "react-redux";
import {createToNews} from "../../store/actions";

class NewPost extends Component {

    createNews = data => {
        this.props.createNews(data).then(() => {
            this.props.history.push('/')
        })
    };
    render() {
        return (
            <Fragment>
                <Container>
                    <h2>New Post</h2>
                    <NewsAddForm createHandler={this.createNews}/>
                </Container>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createNews: data => dispatch(createToNews(data))
});


export default connect(null,mapDispatchToProps)(NewPost);