import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class NewsAddForm extends Component {
    state = {
        title: '',
        content: '',
        image: ''
    };

    submitFormHandler = event => {
        event.preventDefault()

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        })

        this.props.createHandler(formData)
    };

    valueChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };
    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Title</Label>
                    <Col sm={10}>
                        <Input value={this.state.name} onChange={this.valueChange} type="text" name="title" />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label  sm={2}>Content</Label>
                    <Col sm={10}>
                        <Input value={this.state.content} onChange={this.valueChange} type="textarea" name="content"  />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Image</Label>
                    <Col sm={2}>
                        <input type="file" name="image" id="image" onChange={this.fileChangeHandler}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={5}>
                        <Button>Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default NewsAddForm;