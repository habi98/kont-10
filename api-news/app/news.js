const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadsPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = connection => {
    router.get('/', (req,res) => {
        connection.query('SELECT * FROM `news`', (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'})
            }
            res.send(results)
        })
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `news` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'})
            }
            if (results[0]) {
                res.send(results[0])
            } else {
                res.status(404).send({error: 'news not found'})
            }
        })
    });

    router.post('/', upload.single('image'), (req,res) => {
        const post = req.body;
        console.log(post);
        // const date = new Date().toISOString();
        //
        // if (req.file) {
        //     post.image = req.file.filename
        // }
        connection.query('INSERT INTO  `news` (`title`, `content`, `image`, ) values (?, ?)',
            [post.title,  post.content],
            (error) => {
                if(error) {
                    res.status(500).send({error: 'Database error'})
                }
                res.send({message: 'OK'})
            }
        )
    });

    router.delete('/:id', (req,res) => {
        connection.query('DELETE FROM `news` WHERE `id` = ?', req.params.id, (error) => {
            if (!error) {
                res.send('Deleted successfully')
            } else {
                res.status(500).send({error: 'Deletion is impossible'})
            }
        })
    });

    return router;
};

module.exports = createRouter;

