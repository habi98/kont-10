const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const comments = require('./app/comments');
const news = require('./app/news');

const app = express();

app.use(cors());
app.use(express.json());

app.use(express.static('public'));

const port = 8000;

const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'user',
    password : '1qaz@WSX29',
    database :  'api-news'
});

app.use('/comments', comments(connection));
app.use('/news', news(connection));


connection.connect((err) =>  {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);


    app.listen(port, () => {
        console.log(`Server started on ${port} port`)
    });
});
