import axios from '../axios-news';


export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';

export const fetchPostSuccess = (posts) => ({type: FETCH_POST_SUCCESS, posts});

export const  fetchToPostSuccess = () => {
    return dispatch => {
        axios.get('/news').then(response => {
            dispatch(fetchPostSuccess(response.data))
        })
    }
};


export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';

export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});

export const createToNews = (data) => {
    return dispatch => {
       return axios.post('/news', data).then(() => {
            dispatch(createNewsSuccess())
        })
    }
};