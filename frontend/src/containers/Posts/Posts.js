import React, {Component, Fragment} from 'react';
import {Link} from "react-router-dom";
import {Button, Card, CardBody} from "reactstrap";
import {connect} from "react-redux";
import {fetchToPostSuccess} from "../../store/actions";

class Posts extends Component {

    componentDidMount() {
        this.props.fetchToPosts()
    }

    render() {
        return (
          <Fragment>
              <h2 className="pt-4 pb-4">Posts
              <Link  to="/posts/new">

                  <Button

                      color="primary"
                      className="float-right">
                      Add news post
                  </Button>

              </Link>
              </h2>
              {this.props.news.map((news, id) => {
                 return (
                     <Card key={id} style={{marginBottom: '20px'}}>
                         <CardBody>
                             {news.title}
                         </CardBody>
                     </Card>
                 )
              })}
          </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    news: state.news
});

const mapDispatchToProps = dispatch => ({
    fetchToPosts: () => dispatch(fetchToPostSuccess())
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);